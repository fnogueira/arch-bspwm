#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c Brazil -a 12 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

sudo pacman-key --populate

git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm

paru -Syy

paru -S lightdm-settings lightdm-slick-greeter polybar --needed --noconfirm

echo "PACOTES PRINCIPAIS"

sudo pacman -Syy

sleep 5

sudo pacman -S --noconfirm --needed bspwm sxhkd picom nitrogen dmenu alacritty arandr git rofi archlinux-wallpaper neofetch python-pywal thunar thunar-archive-plugin xclip pacman-contrib dunst scrot lxappearance xfce4-settings wget unzip rxvt-unicode lightdm alsa-utils pulseaudio pulseaudio-alsa pavucontrol npm xorg pkgfile flameshot gpick feh file-roller

sudo systemctl enable lightdm

mkdir -p $HOME/.config/{bspwm,sxhkd,polybar}

install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
install -Dm644 /usr/share/doc/polybar/config ~/.config/polybar/config

echo 'setxkbmap -model abnt2 -layout br -variant abnt2' >> ~/.config/bspwm/bspwmrc

sleep 5
printf "\e[1;32mALTERE OS ARQUIVOS NECESSÁRIOS ANTES DE REINICIAR!\e[0m"
